const inventory = require("../inventory");
const FindtheId = require("../Problem/Problem1");

//now here we are calling a function id which will return the id
const result = FindtheId(inventory);

console.log(result);

//if the inventory is not empty then only display the info
if (result != -1) {
  console.log(
    `"Car ${result.id} is a ${result.car_year}  ${result.car_make} ${result.car_model}`
  );
} else {
  // else show the message that inventoy is empty
  console.log("Cars inventory is empty");
}

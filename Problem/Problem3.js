// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

const SortAlphabetiically = (inventory) => {
  if (inventory.length > 0) {
    // if we have cars inside inventory it will sort them alphabetically

    //it will give us the array of car models
    const carModels = inventory.map((car) => car.car_model);

    //here we are doing sorting on the car models of car list
    carModels.sort((first, second) => {
      //it will take everytime two element and sort them according to alphabetically

      let a = first.toLowerCase();
      let b = second.toLowerCase();

      if (a < b) {
        return -1;
      }

      if (a > b) {
        return 1;
      }

      return 0;
    });

    // console.log(carModels);

    return carModels;
  } else {
    //our car inventory is empty so we will not perform sorting here
    return -1;
  }
};

module.exports = SortAlphabetiically;

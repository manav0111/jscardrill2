// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.

const BmwAndAudiCars = (inventory) => {
  let result = [];

  if (inventory.length > 0) {
    //means we have car present inside car inventory

    //so we will loop our inventory array

    //now we will search for only those car's which are BMW or Audi
    result = inventory.filter((element) => {
      //those cars which are  BMW or Audi we will filter them into our result array

      return element.car_make == "BMW" || element.car_make == "Audi";
    });

    return result;
  } else {
    //no car is present in our car inventory
    return -1;
  }
};

module.exports = BmwAndAudiCars;

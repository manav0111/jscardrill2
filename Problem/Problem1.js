// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:

const FindtheId = (inventory) => {
  let result;

  // we will find in only if the inventory is not empty
  if (inventory.length > 0) {
    result = inventory.filter((element) => {
      return element.id == 33;
    });

    //   console.log(result);
    return result[0];
  } else {
    // if we don't have any element in inventory then we will return -1 shows it is not present
    return -1;
  }
};

module.exports = FindtheId;

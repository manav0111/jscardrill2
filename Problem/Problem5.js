// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
const AllCarYears=require('../Problem/Problem4')


const CarsBeforeTwoThousand = (inventory) => {
  let result = [];

  if (inventory.length > 0) {
    //we have cars in our inventory

    //so we are calling AllCarsYears to get the Years of Car's

      let AllYears=AllCarYears(inventory);

    result = AllYears.filter((element) => {
      //we will return this car in our result array
      return element<2000;
    });

    console.log(result.length);
    return result;
  } else {
    return -1;
  }
};

module.exports = CarsBeforeTwoThousand;

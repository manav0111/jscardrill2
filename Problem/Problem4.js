// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

const AllCarYears = (inventory) => {
  let result = [];

  if (inventory.length > 0) {
    //so we have car data inside inventory so we will loop our inventory array

    let result = [];

    inventory.map((element) => {
      //so here we are pushing all the year's inside our result array
      result.push(element.car_year);
    });

    console.log(result);

    //now we are extracting the unique years only
    let uniqueyears = [...new Set(result)];
    return uniqueyears;
  } else {
    return -1;
  }
};

module.exports = AllCarYears;
